﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ConveyorBeltMovement : MonoBehaviour
{

    public float ParallaxSpeed = 0;

    protected GameObject _clone;

    protected Vector3 _movement;
    protected Vector3 _initialPosition;
    protected float _width;

    /// <summary>
    /// On start, we store various variables and clone the object
    /// </summary>
    protected virtual void Start()
    {
        SpriteRenderer sr = this.GetComponent<SpriteRenderer>();
        _width = 2 * sr.sprite.bounds.extents.x * this.transform.localScale.x;
        _initialPosition = transform.position;
        float newPosX = transform.position.x - _width;
        // we clone the object and position the clone at the end of the initial object
        _clone = (GameObject)Instantiate(gameObject, new Vector3(newPosX, transform.position.y, transform.position.z), transform.rotation);
        // we remove the parallax component from the clone to prevent an infinite loop
        Destroy(_clone.GetComponent<ConveyorBeltMovement>());

        _movement = Vector3.right * (ParallaxSpeed / 10) * Time.deltaTime;

        //this.transform.DOMoveX(this.transform.position.x + 0.25f, 1f).SetEase(Ease.InBounce);
        //_clone.transform.DOMoveX(_clone.transform.position.x + 0.25f, 1f).SetEase(Ease.InBounce);
    }

    /// <summary>
    /// On Update, we move the object and its clone
    /// </summary>
    protected virtual void Update()
    {
        // we determine the movement the object and its clone need to apply, based on their speed and the level's speed

        //if (LevelManager.Instance != null)
        //{
        //    _movement = Vector3.left * (ParallaxSpeed / 10) * LevelManager.Instance.Speed * Time.deltaTime;
        //}
        //else
        //{
        //    _movement = Vector3.left * (ParallaxSpeed / 10) * Time.deltaTime;
        //}

        // we move both objects
        _clone.transform.Translate(_movement);
        transform.Translate(_movement);

        // if the object has reached its left limit, we teleport both objects to the right
        if (transform.position.x - 1f * _width >= _initialPosition.x)
        {
            //transform.Translate(Vector3.right * _width * 1f);
            transform.position = _clone.transform.position - new Vector3(_width, 0.0f, 0.0f);
        }
        else if (_clone.transform.position.x - 1f * _width >= _initialPosition.x)
        {
            _clone.transform.position = this.transform.position - new Vector3(_width, 0.0f, 0.0f);
        }
    }
}