﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tmpBeltScript : MonoBehaviour {

    void Start()
    {
        Debug.Log(this.transform.name + " is visible? " + this.GetComponent<SpriteRenderer>().isVisible);
    }

    void OnBecameVisible()
    {
        Debug.Log(this.transform.name + " became visible.");
    }

    void OnBecameInvisible()
    {
        Debug.Log(this.transform.name + " became invisible.");
    }
}
