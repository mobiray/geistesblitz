﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardController : MonoBehaviour {

    public bool shouldMove = false;


    void Start()
    {

    }

	void Update () {

        if (shouldMove)
        {
            //if (LevelManager.Instance != null)
            //{
            //    _movement = Vector3.left * (ParallaxSpeed / 10) * LevelManager.Instance.Speed * Time.deltaTime;
            //}
            //else
            //{
            //    _movement = Vector3.left * (ParallaxSpeed / 10) * Time.deltaTime;
            //}

            // we move both objects
            transform.Translate(_movement);

            // if the object has reached its left limit, we teleport both objects to the right
            if (transform.position.x - 1f * _width >= _initialPosition.x)
            {
                //transform.Translate(Vector3.right * _width * 1f);
                transform.position = _clone.transform.position - new Vector3(_width, 0.0f, 0.0f);
            }
            else if (_clone.transform.position.x - 1f * _width >= _initialPosition.x)
            {
                _clone.transform.position = this.transform.position - new Vector3(_width, 0.0f, 0.0f);
            }
        }
	}
}
