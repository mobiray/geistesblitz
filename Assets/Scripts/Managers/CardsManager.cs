﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class CardsManager : MonoBehaviour {

    public GameObject cardGO;

    private SpriteRenderer cardSR;
    private Vector3 cardInitPosition;
    private string cardName;
    private float cardOffsetX = -0.25f;

    public class Deck
    {
        public List<String> cards = new List<string>();
    }

    protected Deck deckModel = new Deck();
    public Deck currentDeck { get; protected set; }

    void Awake()
    {
        deckModel.cards.Add("CardSample1");
        deckModel.cards.Add("CardSample2");
        deckModel.cards.Add("CardSample3");

        cardSR = cardGO.GetComponent<SpriteRenderer>();
        RectTransform canva = GameObject.Find("Canvas").GetComponent<RectTransform>();
        cardInitPosition = new Vector3(cardOffsetX-cardSR.sprite.bounds.extents.x - (5f * Screen.width / Screen.height), -1.25f, 0f);
    }

    void Start()
    {
        GetNewCurrentDeck();
        PlayCard();
    }

    private void GetNewCurrentDeck()
    {
        Deck tmpDeck = new Deck();
        for(int i=0; i<deckModel.cards.Count; i++)
        {
            tmpDeck.cards.Add(deckModel.cards[i]);
        }
        currentDeck = new Deck();

        int cardsCount = tmpDeck.cards.Count;
        int cardID = 0;
        for (int i = 0; i < cardsCount; i++)
        {
            cardID = UnityEngine.Random.Range(0, tmpDeck.cards.Count);
            cardName = tmpDeck.cards[cardID];
            tmpDeck.cards.RemoveAt(cardID);
            currentDeck.cards.Add(cardName);
        }
    }

    private string GetNewCard()
    {
        if(currentDeck.cards.Count == 0)
        {
            GetNewCurrentDeck();
        }
        cardName = currentDeck.cards[0];
        currentDeck.cards.RemoveAt(0);
        Debug.Log("New card is " + cardName);
        return cardName;
    }

    public void PlayCard()
    {
        cardGO.transform.position = cardInitPosition;
        cardSR.sprite = Resources.Load<Sprite>("CardsLibrary/" + GetNewCard());

    }

}
