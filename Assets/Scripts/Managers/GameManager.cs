﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    public int totalLives { get; protected set; }
    public int currentLives { get; protected set; }
    public int score { get; protected set; }

    static public GameManager Instance { get { return _instance; } }
    static protected GameManager _instance;
    void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else if (_instance != this)
        {
            Destroy(gameObject);
        }
    }

    public void DeductLive()
    {
        currentLives--;
    }

    public void AppendLive()
    {
        currentLives++;
    }
}
